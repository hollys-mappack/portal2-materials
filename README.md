# portal2-materials

[Portal 2](https://steamdb.info/app/620/)'s textures, converted from 7.5 to 7.4 with [VTFVer](https://developer.valvesoftware.com/wiki/VTFVer)

For use in Sourcemods running on SDK2013 that want to mount Portal 2's textures.

All rights belong to Valve Corporation.
